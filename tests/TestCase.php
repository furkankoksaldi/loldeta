<?php

namespace Barryvdh\Cors\Tests;

use Illuminate\Routing\Router;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    use \Illuminate\Foundation\Validation\ValidatesRequests;

    protected function resolveApplicationConfiguration($app)
    {
        parent::resolveApplicationConfiguration($app);

        $app['config']['cors'] = [
            'supportsCredentials' => false,
            'allowedOrigins' => ['localhost'],
            'allowedHeaders' => ['X-Custom-1', 'X-Custom-2'],
            'allowedMethods' => ['GET', 'POST'],
            'exposedHeaders' => [],
            'maxAge' => 0,
        ];
    }





}
