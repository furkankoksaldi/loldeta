<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return redirect()->route('home');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home','HomeController@lol_verileri');

Route::post('/lolapi', 'HomeController@lolApi')->name('lolapicek');
Route::post('/sampiyonapi','HomeController@sampiyonApi')->name('sampiyon_bilgileri');
Route::post('/spellapi','HomeController@spellApi')->name('spell_bilgisi');
Route::post('/runapi','HomeController@runApi')->name('run_bilgisi');
