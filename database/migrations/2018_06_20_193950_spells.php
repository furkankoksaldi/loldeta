<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Spells extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('spells', function (Blueprint $table) {
           $table->increments('id');
           $table->string('name');
           $table->string('key');
           $table->text('description');
           $table->integer('summonerLevel');
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('spells');
     }
}
