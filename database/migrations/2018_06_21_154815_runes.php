<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Runes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('runler', function (Blueprint $table) {
           $table->increments('id');
           $table->string('name');
           $table->string('key');
           $table->string('icon');
           $table->string('runePathName');
           $table->text('longDesc');
           $table->text('shortDesc');
           $table->integer('runePathId');
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('runler');
     }
}
