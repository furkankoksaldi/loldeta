@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    {{ Form::open(array(  'action' => ["HomeController@lol_verileri"] ,'method' => 'post', 'class' => 'form-horizontal' )) }}
                    <div class="form-group">
                      {{ Form::label('', "Grup Adı : " , array('class' => 'col-md-4 control-label')) }}
                      <div class="col-md-6">
                        {{ Form::text('adi' , "" , array('class'=>'form-control'))}} <!-- 1. kutu -->
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-8 col-md-offset-4">
                        {{ Form::select('server_adi', ['ru' => 'RU', 'kr' => 'KR', 'br1' => 'BR1', 'oc1' => 'OC1', 'jp1' => 'JP1', 'na1' => 'NA1', 'eun1' => 'EUN1' , 'euw1' => 'EUW1', 'tr1' => 'TR1', 'la1' => 'LA1', 'la2' => 'LA2']) }}
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-8 col-md-offset-4">
                        {{ Form::submit("Ekle", array('class' => 'btn btn-primary'))}}
                      </div>
                    </div>
                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
