@extends('layouts.app')

@section('content')
<div id="arkaplan" class="arkaplan" style="background:#363636">
  <div id="loadinggif" class="loader"></div>

</div>

<div class="container" >
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header" style="background:#363636">Dashboard</div>
        <div class="card-body" style="background:#363636">
          @if (session('status'))
          <div class="alert alert-success">
            {{ session('status') }}
          </div>
          @endif


          <div class="container">
            <div class="row">
              <div id="champion0" class="col-6" >
                <img id="prfId0" src="" height="65px" alt="">
                <img id="cImg0" src="" height="65px" alt="">
                <img id="prf1-0" src="" height="30px"  alt="">
                <img id="prf2-0" src="" height="30px" alt="">
                <span id=cName0></span>
              </div>
              <div id="champion1" class="col-6">
                <img id="prfId5" src="" height="65px" alt="">
                <img id="cImg5" src="" height="65px" alt="">
                <img id="prf1-5" src="" height="30px"  alt="">
                <img id="prf2-5" src="" height="30px" alt="">
                <span id=cName5></span>
              </div>
              <div id="champion2" class="col-6">
                <img id="prfId1" src="" height="65px" alt="">
                <img id="cImg1" src="" height="65px" alt="">
                <img id="prf1-1" src="" height="30px"  alt="">
                <img id="prf2-1" src="" height="30px" alt="">
                <span id=cName1></span>
              </div>
              <div id="champion3" class="col-6">
                <img id="prfId6" src="" height="65px" alt="">
                <img id="cImg6" src="" height="65px" alt="">
                <img id="prf1-6" src="" height="30px"  alt="">
                <img id="prf2-6" src="" height="30px" alt="">
                <span id=cName6></span>
              </div>
              <div id="champion4" class="col-6">
                <img id="prfId2" src="" height="65px" alt="">
                <img id="cImg2" src="" height="65px" alt="">
                <img id="prf1-2" src="" height="30px"  alt="">
                <img id="prf2-2" src="" height="30px" alt="">
                <span id=cName2></span>
              </div>
              <div id="champion5" class="col-6">
                <img id="prfId7" src="" height="65px" alt="">
                <img id="cImg7" src="" height="65px" alt="">
                <img id="prf1-7" src="" height="30px"  alt="">
                <img id="prf2-7" src="" height="30px" alt="">
                <span id=cName7></span>
              </div>
              <div id="champion6" class="col-6">
                <img id="prfId3" src="" height="65px" alt="">
                <img id="cImg3" src="" height="65px" alt="">
                <img id=prf1-3 src="" height="30px"  alt="">
                <img id="prf2-3" src="" height="30px" alt="">
                <span id=cName3></span>
              </div>
              <div id="champion7" class="col-6">
                <img id="prfId8" src="" height="65px" alt="">
                <img id="cImg8" src="" height="65px" alt="">
                <img id=prf1-8 src="" height="30px"  alt="">
                <img id="prf2-8" src="" height="30px" alt="">
                <span id=cName8></span>
              </div>
              <div id="champion8" class="col-6">
                <img id="prfId4" src="" height="65px" alt="">
                <img id="cImg4" src="" height="65px" alt="">
                <img id="prf1-4" src="" height="30px"  alt="">
                <img id="prf2-4" src="" height="30px" alt="">
                <span id=cName4></span>
              </div>
              <div id="champion9" class="col-6">
                <img id="prfId9" src="" height="65px" alt="">
                <img id="cImg9" src="" height="65px" alt="">
                <img id=prf1-9 src="" height="30px"  alt="">
                <img id="prf2-9" src="" height="30px" alt="">
                <span id=cName9></span>
              </div>

            </div>
            <p>Banlanan şampiyonlar</p>
            <div class="row">
              <img id="bImg0" src="" alt="">
              <img id="bImg1" src="" alt="">
              <img id="bImg2" src="" alt="">
              <img id="bImg3" src="" alt="">
              <img id="bImg4" src="" alt="">
              <img id="bImg5" src="" alt="">
              <img id="bImg6" src="" alt="">
              <img id="bImg7" src="" alt="">
              <img id="bImg8" src="" alt="">
              <img id="bImg9" src="" alt="">
            </div>
            <p>Rünler</p>
            <?php
            for ($i=0; $i < 10; $i++) {
              echo "<div class='row'>";
              echo "<p id='name" . $i . "'></p>";
              for ($l=0; $l < 6; $l++) {
                echo "<img id='name" . $i . "run" . $l . "' src='' height='65px' alt=''>";
              }
              for ($j=0; $j < 2; $j++) {
                echo "<img id='name" . $i . "run_path_" . $j . "' src='' height='65px' alt=''>";
              }

              echo "</div>";
            }

              ?>


          </div>





        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">


  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $.ajax({
    url:  "{{ route('lolapicek') }}",
    type: 'POST',
    data: {
      nick : "{{ $nick }}",
      game_server : "{{ $server }}"
    },
    error: function(error) {
      console.log(error);
      $("#arkaplan").hide();

    },
    beforeSend: function () {
      // $("#imgSpinner1").show();
      console.log('yükleniyor');
      $("#arkaplan").show();

    },
    // hides the loader after completion of request, whether successfull or failor.
    complete: function () {
      console.log('tamamlandı');
      $("#arkaplan").hide();

        },
    success: function(data) {
      console.log(data);
      for (var i = 0; i < 10; i++) {
        console.log(data);
        profilIcons(data.mac_verisi.participants[i].profileIconId,"prfId"+i);
        sampiyonlar(data.mac_verisi.participants[i].championId,"cImg"+i);
        spells(data.mac_verisi.participants[i].spell1Id,"prf1-"+i);
        spells(data.mac_verisi.participants[i].spell2Id,"prf2-"+i)
        if (data.mac_verisi.bannedChampions[i] != null) {
          sampiyonlar(data.mac_verisi.bannedChampions[i].championId,"bImg"+i);
        }
        var perks = data.mac_verisi.participants[i].perks.perkIds;
        for (var variable in perks) {
          runs(perks[variable],"name" + i +"run" + variable,"run");

        }
        runs(data.mac_verisi.participants[i].perks.perkStyle,"name" + i + "run_path_0","run_pat");
        runs(data.mac_verisi.participants[i].perks.perkSubStyle,"name" + i + "run_path_1","run_pat");

        document.getElementById("cName"+i).innerHTML = data.mac_verisi.participants[i].summonerName;
        document.getElementById("name"+i).innerHTML = data.mac_verisi.participants[i].summonerName;
      }
    },
  });

  function runs(id,divId,runs) {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "{{ route('run_bilgisi') }}",
      type: 'POST',
      data: {
        id:id,
        divId:divId,
        runs:runs
      },
      success: function(data){
        console.log(data);
        var elem = document.getElementById(divId);
        elem.src = "./dragontail-8.13.1/img/" + data.run.icon;
      },
      error: function(error){
        console.log(error);
      }
    });
  }



  function spells(id,divId) {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "{{ route('spell_bilgisi') }}",
      type: 'POST',
      data: {
        id:id
      },
      success: function(data){
        var elem = document.getElementById(divId);
        elem.src = "./dragontail-8.13.1/8.13.1/img/" + data.smp.image.group + "/" + data.smp.image.full;
      },
      error: function(error){
        console.log(error);
      }
    });
  }



  function profilIcons(id,divId) {
      var elem = document.getElementById(divId);
      elem.src = "./dragontail-8.13.1/8.13.1/img/profileicon/" + id + ".png";
  }

  function sampiyonlar(id,divId) {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "{{ route('sampiyon_bilgileri') }}",
      type: 'POST',
      data: {
        id:id
      },
      success: function(data){
        if (data == "notChamp") {
          console.log("data Yok");
        }
        else if (data.smp.key != null) {
          var elem = document.getElementById(divId);
          elem.src = "./dragontail-8.13.1/8.13.1/img/" + data.smp.image.group + "/" + data.smp.image.full;
          elem.style = "height:65px";
        }


        // console.log( data.smp.image.full);
      },
      error: function(error){
        console.log(error);
      }
    });
  }
</script>


@endsection
