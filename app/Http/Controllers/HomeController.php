<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sampiyonlar;
use App\Spells;
use App\Runler;

class HomeController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  // public function __construct()
  // {
  //   $this->middleware('auth');
  // }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    return view('home');
  }
  public function lol_verileri(Request $request)
  {


    return view('bilgiler',['nick' => $request->adi,'server' => $request->server_adi]);
  }

  public function lolApi(Request $request)
  {
    $key = 'RGAPI-b42be9e3-4817-4adc-80ed-a662f948c193';
    $nick = str_replace('+','%20',urlencode($request->nick));
    $server = $request->game_server;
    $oyuncu_url = 'https://' . $server . '.api.riotgames.com/lol/summoner/v3/summoners/by-name/' . $nick . '?api_key=' . $key;
    $oyuncu_data = file_get_contents($oyuncu_url);
    $gelen_oyuncu_verisi = json_decode($oyuncu_data);

    $mac_url = 'https://' . $server . '.api.riotgames.com/lol/spectator/v3/active-games/by-summoner/' . $gelen_oyuncu_verisi->id . '?api_key=' . $key;
    $mac_datası = file_get_contents($mac_url);
    $gelen_mac_verisi = json_decode($mac_datası);
    return response()->json([
      'mac_verisi' => $gelen_mac_verisi,
      'oyuncu_verisi' => $gelen_oyuncu_verisi,
    ]);



  }

  public function sampiyonApi(Request $request)
  {

    $str = file_get_contents('./dragontail-8.13.1/8.13.1/data/tr_TR/champion.json');
    $json = json_decode($str);
    foreach ($json->data as $key => $value) {
      if ($value->key == $request->id) {
        return response()->json([
          'smp' => $value,
        ]);
      }
    }
    return "noChampion";
    // $smp = Sampiyonlar::where('id',$request->id)->first();
    // return $smp;
  }



  public function spellApi(Request $request)
  {
    $str = file_get_contents('./dragontail-8.13.1/8.13.1/data/tr_TR/summoner.json');
    $json = json_decode($str);
    foreach ($json->data as $key => $value) {
      if ($value->key == $request->id) {
        return response()->json([
          'smp' => $value,
        ]);
      }
    }
    return "noChampion";

    // $spl = Spells::where('id',$request->id)->first();
    // return $spl;
  }

  public function runApi(Request $request)
  {
    $str = file_get_contents('./dragontail-8.13.1/8.13.1/data/tr_TR/runesReforged.json');
    $json = json_decode($str);
    if ($request->runs == "run") {
      foreach ($json as $key => $value) {
        foreach ($value->slots as $key2 => $value2) {
          foreach ($value2->runes as $key3 => $value3) {
            if ($value3->id == $request->id) {
               return response()->json([
                 'run' => $value3,
               ]);
             }
          }
        }
      }
    }

    elseif ($request->runs == "run_pat") {
      foreach ($json as $key => $value) {
        if ($value->id == $request->id) {
          return response()->json([
            'run' => $value,
          ]);
        }
      }
    }
    return "noRun";

    // $run = Runler::where('id',$request->id)->first();
    // return $run;
  }



}
